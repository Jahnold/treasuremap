// JavaScript to create a google map element
// It then loads gazateer data from several fusion tables
// Controls allows switching layers on/off and searching

var road_list = new Array('A1', 'A10', 'A1001', 'A1033', 'A1053', 'A1057', 'A1081', 'A1089', 'A11', 'A12', 'A120', 'A127', 'A13', 'A133', 'A14', 'A14M', 'A15', 'A160', 'A168', 'A174', 'A180', 'A184', 'A19', 'A194M', 'A1M', 'A2', 'A20', 'A2070', 'A21', 'A228', 'A229', 'A23', 'A24', 'A249', 'A259', 'A26', 'A27', 'A282', 'A3', 'A30', 'A303', 'A308M', 'A31', 'A316', 'A329M', 'A34', 'A35', 'A36', 'A361', 'A38', 'A38M', 'A39', 'A3M', 'A4', 'A40', 'A404', 'A404M', 'A405', 'A406', 'A41', 'A414', 'A417', 'A419', 'A42', 'A421', 'A428', 'A43', 'A446', 'A449', 'A45', 'A452', 'A453', 'A458', 'A46', 'A47', 'A483', 'A49', 'A5', 'A50', 'A500', 'A5036', 'A5103', 'A5111', 'A5117', 'A5148', 'A52', 'A55', 'A550', 'A556', 'A56', 'A57', 'A570', 'A585', 'A59', 'A590', 'A595', 'A6', 'A61', 'A6129', 'A614', 'A616', 'A627M', 'A628', 'A63', 'A631', 'A64', 'A65', 'A66', 'A663', 'A66M', 'A67', 'A69', 'A74', 'A74M', 'M1', 'M11', 'M18', 'M180', 'M181', 'M2', 'M20', 'M23', 'M25', 'M26', 'M27', 'M271', 'M275', 'M3', 'M32', 'M4', 'M40', 'M42', 'M45', 'M48', 'M49', 'M5', 'M50', 'M53', 'M54', 'M55', 'M56', 'M57', 'M58', 'M6', 'M60', 'M602', 'M606', 'M61', 'M62', 'M621', 'M65', 'M66', 'M67', 'M69', 'M6T');


$(document).ready(function () {

    var $body = $('body');

    // set up road list for gaz list
    $.each(road_list, function (key, road) {

        $('#gazlistroadlist').append($('<option>', { value: road}).text(road));

    });

    // set up jquery ui items
    $( ".accordion" ).accordion({
        active: false,
        heightStyle: "content",
        collapsible: true,
	    activate: function(event, ui) {
   	        resizeGazList()
        }
    });
    
    $( ".accordionfill" ).accordion({
        active: false,
        heightStyle: "content",
        collapsible: true,
        activate: function(event, ui) {
            resizeGazList()
        }
    });

    function resizeGazList() {

        var $gazlist = $('#gazlist');
        var top = $gazlist.offset().top;
        var height = window.innerHeight - top - 25;
        $gazlist.css('height', height + "px");
    }

    $(window).resize(function(){
        resizeGazList()
    });


    $('.jah-dialog').draggable({handle: ".jah-dialog-toolbar"});
    $('#streetview-container').resizable({
        stop: function(event, ui) {
            google.maps.event.trigger(panorama,'resize');
        }
    });

    $('button').button();

    // initialise the map
    var markers = [];
    var layers = [];
    var myOptions = {
        center: new google.maps.LatLng(52.679713, -1.159058),
        zoom: 8,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        scaleControl: true
    };

    var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

    // initialise the direction service
    directionService = new google.maps.DirectionsService();

    // initialise an infoWindow
    var infoWindow = new google.maps.InfoWindow();

    // create a helper function for it
    function setInfoWindow(content, position, offset) {

        // loop through object and make a title: value output for each one
        var contentStr = '<div class="googft-info-window">';
        $.each(content, function(title,value) {
            if (title != 'Link') {
                contentStr += '<b>' + title + ': </b>' + value + '<br/>'
            }
            else {
                // unless it's the link which is a special case
                contentStr += '<a href="#" data-value="' + value.dataValue + '" class="' + value.className + '">' + value.text + '</a>';
            }
        });
        contentStr += '</div>';

        // making it a jQuery object fixes scrollbar issue for infoSindow
        var $contentStr = $(contentStr);

        // set up the infoWindow options
        infoWindow.setOptions({
            content: $contentStr[0],
            position: position,
            pixelOffset: offset
        });

        // display the infoWindow
        infoWindow.open(map);
    }


    //***Initialise Layers***
    //____________________________


    // Gazateer Locations
    layers['gaz'] = new google.maps.FusionTablesLayer({
        query: {
            select: 'LatLong',
            from: '18w9JMwql8boPTSr3hc8_4kxstbumiPkUTGM5mDk'
        },
        suppressInfoWindows: true
    });

    // add a listener for clicks on this layer
    google.maps.event.addListener(layers['gaz'], 'click', function(event) {

        setInfoWindow({
            'Gazetteer Name': event.row['Gazateer Name'].value,
            'Road': event.row['Road'].value,
            'Area': event.row['Area'].value,
            'Region': event.row['Region'].value,
            'Police':event.row['Police'].value
        },event.latLng,event.pixelOffset);
    });

    // add to map by default
    layers['gaz'].setMap(map);

    // ERTs
    layers['ert'] = new google.maps.FusionTablesLayer({
        query: {
            select: 'LatLong',
            from: '1g6xMKUUPHJ_grFIQfN8iVNMtMl4XCopubO4FzKU'
        },
        styleId: 2,         // need these to load style from fusion table
        templateId: 2,
        suppressInfoWindows: true
    });

    // add a listener for clicks on this layer
    google.maps.event.addListener(layers['ert'], 'click', function(event) {

        setInfoWindow({
            'ERT': event.row['ERT'].value,
            'Location': event.row['Link'].value,
            'Road': event.row['Road'].value,
            'Area': event.row['Area'].value,
            'Region': event.row['RCC'].value,
            'Police':event.row['Police'].value,
            'Link': {
                'className': 'view-on-streetview',
                'text': 'Show on StreetView',
                'dataValue': event.row['LatLong'].value
            }
        },event.latLng,event.pixelOffset);
    });

    // Markers
    layers['mp'] = new google.maps.FusionTablesLayer({
        query: {
            select: 'LatLong',
            from: '1DBsPJZeg8vXnycMMHD3onqr7yGZ4jjGkTzuafSc'
        },
        styleId: 2,         // need these to load style from fusion table
        templateId: 2,
        suppressInfoWindows: true
    });

    // add a listener for clicks on this layer
    google.maps.event.addListener(layers['mp'], 'click', function(event) {

        setInfoWindow({
            'Marker': event.row['BD'].value,
            'Location': event.row['TN'].value,
            'Road': event.row['DD'].value,
            'Area': event.row['Area'].value,
            'Region': event.row['Region'].value,
            'Police':event.row['CN'].value,
            'Link': {
                'className': 'view-on-streetview',
                'text': 'Show on StreetView',
                'dataValue': event.row['LatLong'].value
            }
        },event.latLng,event.pixelOffset);
    });

    // cameras
    layers['cam'] = new google.maps.FusionTablesLayer({
        query: {
            select: 'LatLng',
            from: '1TwXB87plsulUK_zyvKDKWebKA6ZY69bO9t3Gxrw'
        },
        styleId: 2,
        templateId: 2,
        suppressInfoWindows: true
    });

    // add a listener for clicks on this layer
    google.maps.event.addListener(layers['cam'], 'click', function(event) {

        setInfoWindow({
            'Camera': event.row['Camera'].value,
            'Location': event.row['Location'].value,
            'Road': event.row['Road'].value,
            'Area': event.row['Area'].value,
            'Region': event.row['Region'].value,
            'Police':event.row['Police'].value,
            'Link': {
                'className': 'view-camera',
                'text': 'View Camera',
                'dataValue': event.row['Camera'].value
            }
        },event.latLng,event.pixelOffset);
    });

    // VMS
    layers['vms'] = new google.maps.FusionTablesLayer({
        query: {
            select: 'LatLng',
            from: '1f2DFLHGdNkE3d3olrtwh-e-dqJFTRmI862AzPHo'
        },
        styleId: 2,
        templateId: 2,
        suppressInfoWindows: true
    });

    // add a listener for clicks on this layer
    google.maps.event.addListener(layers['vms'], 'click', function(event) {

        setInfoWindow({
            'VMS': event.row['VMS'].value,
            'Size': event.row['Size'].value
        },event.latLng,event.pixelOffset);
    });

    // Traffic Layer
    layers['traffic'] = new google.maps.TrafficLayer();

    layers['weather'] = new google.maps.weather.WeatherLayer({
        temperatureUnits: google.maps.weather.TemperatureUnit.CELSIUS
    });

    // Distance/Direction Layer
    layers['distance'] = new google.maps.DirectionsRenderer({
        draggable: true,
        markerOptions: {
            draggable: true
        }
    });

    //***Layer Checkboxes***
    //______________________


    $('.layer-switch').click(function() {
        var $this = $(this);
        if ($this.prop('checked')) {
            // box is now checked...show layer
            layers[$this.attr('id')].setMap(map);
        }
        else {
            // box is not checked...hide layer
            layers[$this.attr('id')].setMap(null);
        }
    });


    //***ERT/MP Search***
    //________________

    $("#btn-feature-search").click(function () {

        // get the search text from the input
        var searchQuery = $("#ertsearch").val().toUpperCase();
        var searchArray = searchQuery.split(",");

        // make an array for our search query and start it off
        var queryArray = [];
        queryArray[0] = "SELECT * FROM ";

        var searchOptions = [];
        searchOptions['T'] = {
            'table': "1g6xMKUUPHJ_grFIQfN8iVNMtMl4XCopubO4FzKU ",
            'column': "ERT",
            'road': 'Road',
            'info': function(location,resultLocation) {
                setInfoWindow({
                    'ERT': location[0],
                    'Location': location[1],
                    'Road': location[3],
                    'Area': location[2],
                    'Region': location[7],
                    'Police':location[4],
                    'Link': {
                    'className': 'view-on-streetview',
                        'text': 'Show on StreetView',
                        'dataValue': location[8]
                    }
                },resultLocation,new google.maps.Size(0,-25));
            }
        };
        searchOptions['P'] = {
            'table': "1DBsPJZeg8vXnycMMHD3onqr7yGZ4jjGkTzuafSc ",
            'column': "BD",
            'road': 'DD',
            'info': function(location,resultLocation) {
                setInfoWindow({
                    'Marker': location[0],
                    'Location': location[1],
                    'Road': location[3],
                    'Area': location[2],
                    'Region': location[7],
                    'Police': location[4],
                    'Link': {
                        'className': 'view-on-streetview',
                        'text': 'Show on StreetView',
                        'dataValue': location[8]
                    }
                },resultLocation,new google.maps.Size(0,-25));
            }
        };
        searchOptions['C'] = {
            'table': "1TwXB87plsulUK_zyvKDKWebKA6ZY69bO9t3Gxrw ",
            'column': "Camera",
            'road': 'Road',
            'info': function(location,resultLocation) {
                setInfoWindow({
                    'Camera': location[0],
                    'Location': location[1],
                    'Road': location[3],
                    'Area': location[2],
                    'Region': location[7],
                    'Police':location[4],
                    'Link': {
                        'className': 'view-camera',
                        'text': 'View Camera',
                        'dataValue': location[0].value
                    }
                },resultLocation,new google.maps.Size(0,-25));
            }
        };

        var searchType = searchQuery.substring(0, 1);

        // check to make sure the search string begins with a T,P or C
        if (jQuery.inArray(searchType,["T","P","C"]) > -1) {
            queryArray[1] = searchOptions[searchType].table;
            queryArray[2] = "WHERE " + searchOptions[searchType].column + "='" + searchArray[0] + "' ";
            if (searchArray[1]) {
                queryArray[3] = "AND " + searchOptions[searchType].road + "='" + searchArray[1] + "' ";
            }

            clearMarkers();

            // do an ajax request to the fusion table api to get data
            var queryUrlHead = 'https://www.googleapis.com/fusiontables/v1/query?sql=';
            var queryUrlTail = '&jsonCallback=callback&key=AIzaSyDm7dJBMQHXmOK6XZled79_VCrTLrv4QTM';
            var queryUrl = encodeURI(queryUrlHead + queryArray.join("") + queryUrlTail);

            $.get(queryUrl, function (data) {

                    // set up a bounds object
                    var bounds = new google.maps.LatLngBounds();

                    // add markers on the map for each found location row
                    $.each(data.rows,function(key, location) {

                        // create the location, latlong is always in the 8th column
                        var latlng = location[8].split(",");
                        var resultLocation = new google.maps.LatLng(latlng[0],latlng[1]);

                        // create a marker
                        var marker = new google.maps.Marker({
                            position: resultLocation,
                            map: map
                        });

                        // add the marker to the markers array so that it can be cleared later
                        markers.push(marker);

                        // add the marker to the bounds
                        bounds.extend(resultLocation);

                        // add a click listener for an info window
                        google.maps.event.addListener(marker, 'click', function() {
                            //setInfoWindow(searchOptions[searchType].info,resultLocation,new google.maps.Size(0,0))
                            searchOptions[searchType].info(location,resultLocation);
                        });

                    });

                    // Don't zoom in too far on only one marker
                    if (bounds.getNorthEast().equals(bounds.getSouthWest())) {
                        var extendPoint1 = new google.maps.LatLng(bounds.getNorthEast().lat() + 0.01, bounds.getNorthEast().lng() + 0.01);
                        var extendPoint2 = new google.maps.LatLng(bounds.getNorthEast().lat() - 0.01, bounds.getNorthEast().lng() - 0.01);
                        bounds.extend(extendPoint1);
                        bounds.extend(extendPoint2);
                    }

                    // fit the map to the bounds
                    map.fitBounds(bounds);

                }
                , "jsonp");
        }
        else {

            //
            alert("Please use P for Marker, T for ERT or C for Camera.  Thanks!")
        }
    });

    function clearMarkers() {

        // clear current markers from the map
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(null);
        }
        // and empty the array
        markers = [];
    }

    // user clicks on the clear results button
    $('#btn-clear-search').click(function() {
        clearMarkers();
    });


    //***Gaz List***
    //______________

    // user chooses a road
    $('#gazlistroadlist').change(function () {

        var selected_road = $('#gazlistroadlist').val();
        if (selected_road == "nope") {
            // do nothing
        }
        else {
            // load the list
            var table = '18w9JMwql8boPTSr3hc8_4kxstbumiPkUTGM5mDk';

            // do an ajax request to the fusion table api to get data
            var queryUrlHead = 'https://www.googleapis.com/fusiontables/v1/query?sql=';
            var queryUrlTail = '&jsonCallback=callback&key=AIzaSyDm7dJBMQHXmOK6XZled79_VCrTLrv4QTM';

            // write your SQL as normal, then encode it
            var query = "SELECT 'Gazateer Name', Road, LatLong, RecordType, Region, Area, Police FROM " + table + " WHERE Road='" + selected_road + "' ORDER BY ST_DISTANCE(LatLong, LATLNG(49.0,6.0))";
            var queryurl = encodeURI(queryUrlHead + query + queryUrlTail);

            $.get(queryurl, function (data) {

                    // the data var should now hold our json object full of road data
                    var locations = data.rows;

                    // clear the current list
                    var $gazul = $('#gazul');
                    $gazul.html('');

                    // loop through the new list and add the items
                    for (var j = 0; j < locations.length; j++) {

                        // append list item
                        var liHtml = '<li ';
                        liHtml += 'class="gaz-list-item type' + locations[j][3] + '" ';
                        liHtml += 'data-latlng="' + locations[j][2] + '" ';
                        liHtml += 'data-road="' + locations[j][1] + '" ';
                        liHtml += 'data-region="' + locations[j][4] + '" ';
                        liHtml += 'data-area="' + locations[j][5] + '" ';
                        liHtml += 'data-police="' + locations[j][6] + '" ';
                        liHtml += '>' + locations[j][0] + '</li>';

                        $gazul.append(liHtml);

                    }

                }
                , "jsonp");
        }
    });

    // user clicks on a gazlist item
    $body.on('click', '.gaz-list-item', function() {

        var $this = $(this);

        // centre the map on at the location
        var moveTo = $(this).attr('data-latlng').split(",");

        var centre = new google.maps.LatLng(moveTo[0],moveTo[1]);
        map.panTo(centre);
        map.setZoom(14);

        // create an infowindow for this location
        setInfoWindow({
            'Gazetteer Name': $this.text(),
            'Road': $this.attr('data-road'),
            'Area': $this.attr('data-area'),
            'Region': $this.attr('data-region'),
            'Police': $this.attr('data-police')
        },centre,new google.maps.Size(0, 0));

    });



    //***Gaz List Options***
    //______________________

    $('.glo').click(function () {

        var toggletype = this.id.substring(3, this.id.length);
        $('.type' + toggletype).toggleClass('hidden')

    });

    $('#gazoptionsbtn').click(function () {

        $('#gazlistoptions').slideToggle();
    });


    //***Places Search***
    //________________


    // set up the google places search box
    // Create the search box and link it to the UI element.
    var input = document.getElementById('search');
    var searchBoxOptions = {
            // limit to uk
            componentRestrictions: {country: "uk"}
    };
    var autocomplete = new google.maps.places.Autocomplete(input, searchBoxOptions);
    var marker = new google.maps.Marker({
        map: map
    });

    // Listen for the event fired when the user selects an item from the
    // pick list. Retrieve the matching places for that item.
    google.maps.event.addListener(autocomplete, 'place_changed', function () {
        marker.setVisible(false);
        var place = autocomplete.getPlace();
        if (!place.geometry) {
            return;
        }

        // If the place has a geometry, then present it on a map.
        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);  // Why 17? Because it looks good.
        }
        marker.setIcon(({
            url: place.icon,
            size: new google.maps.Size(71, 71),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(17, 34),
            scaledSize: new google.maps.Size(35, 35)
        }));
        marker.setPosition(place.geometry.location);
        marker.setVisible(true);
    });


    //***Dialog Controls***
    //_____________________

    // user clicks dialog close button
    $('.jah-dialog-close-btn').click(function() {

        var dialogName = $(this).attr('data-dialog');
        $('#' + dialogName + '-dialog').addClass('hidden');
        $('#' + dialogName + '-minimized').addClass('hidden');
    });

    // user clicks dialog minimize button
    $('.jah-dialog-min-btn').click(function() {

        var dialogName = $(this).attr('data-dialog');
        $('#' + dialogName + '-dialog').addClass('hidden');
        $('#' + dialogName + '-minimized').removeClass('hidden');
    });

    // user clicks dialog maximize button
    $('.jah-dialog-max-btn').click(function() {

        var dialogName = $(this).attr('data-dialog');
        $('#' + dialogName + '-dialog').removeClass('hidden');
        $('#' + dialogName + '-minimized').addClass('hidden');
    });


    //***Street View***
    //_________________

    var panoramaOptions = {
        position: new google.maps.LatLng(52.45394,-2.008439),
        pov: {
            heading: 34,
            pitch: 10
        }
    };
    var panorama = new  google.maps.StreetViewPanorama(document.getElementById("streetview-container"), panoramaOptions);

    // user clicks a view on streetview link
    $body.on('click', '.view-on-streetview', function() {

        // get the lat long
        var latLng = $(this).attr('data-value').split(',');

        // set the streetview control to the location
        panorama.setPosition(new google.maps.LatLng(latLng[0],latLng[1]));

        // hide the minimized view if it's there
        $('#streetview-minimized').addClass('hidden');
        // show the dialog
        $('#streetview-dialog').removeClass('hidden');

        // set it visible
        panorama.setVisible(true);
    });


    //***Cameras***
    //_____________

    // user clicks on a view camera link
    $body.on('click', '.view-camera', function() {

        var camNumber = $(this).attr('data-value').substr(1,5);

        // set the source of the iframe
        $('#camera-frame').attr('src','http://www.trafficengland.com/trafficcamera.aspx?cameraUri=http://public.hanet.org.uk/cctvpublicaccess/html/' + camNumber + '.html');

        // show the camera dialog
        $('#camera-minimized').addClass('hidden');
        $('#camera-dialog').removeClass('hidden');
    });


    //***Distance Measure***
    //______________________

    // user clicks the distance measure button
    $('#distance-measure-btn').click(function() {
        var $this = $(this);

        if ($this.hasClass('active')) {
            // switching it off
            $this.removeClass('active');
            map.setOptions({ draggableCursor: null});
            $('#distance-dialog').addClass('hidden');
        }
        else {
            // switching it on
            $this.addClass('active');
            map.setOptions({ draggableCursor: 'crosshair'});
            $('#distance-dialog').removeClass('hidden');
        }


    });

    var distancePoints = [];

    function addDistancePoint(location) {

        distancePoints.push(location);
    }

    function addDistanceListItem(location) {

        var listItem = '<li class="distance-list-item">';
            listItem += location.toString();
            listItem += '<span class="ui-icon ui-icon-close distance-list-remove-btn"></span>';
            listItem += '</li>';

        $('#distance-list').append($(listItem));
    }

    function calcDistance() {

        var numberOfLocations = distancePoints.length;

        // make sure there is more than one point
        if (numberOfLocations >= 2) {

            var origin = distancePoints[0];
            var destination = distancePoints[numberOfLocations - 1];

            // if there are more than 2 locations then the middle ones are waypoints
            // get their location as an array
            if (numberOfLocations > 2) {
                var wayPoints = [];
                for (var i = 1; i < numberOfLocations - 1; i++) {
                    wayPoints.push({
                        location: distancePoints[i],
                        stopover: false
                    });
                }
            }
            // create a request options object
            var directionRequest = {
                origin: origin,
                destination: destination,
                waypoints: wayPoints,
                provideRouteAlternatives: false,
                travelMode: google.maps.TravelMode.DRIVING,
                unitSystem: google.maps.UnitSystem.IMPERIAL
            };

            directionService.route(directionRequest, function(result, status) {
                if (status == google.maps.DirectionsStatus.OK) {
                    layers['distance'].setDirections(result);
                    layers['distance'].setMap(map);
                    updateDistance(result)
                }
            });
        }
    }

    function updateDistance(result) {

        var km, miles;

        // if null has been passed in set both to 0
        if (result == null) {
            km = 0;
            miles = 0;
        }
        // otherwise work out the distances
        else {
            var distance = result.routes[0].legs[0].distance.value;
            km = Math.round(distance / 10) / 100;
            miles = Math.round(distance / 16.09) / 100;
        }

        // set the values in the spans
        $('#distance-km').text(km);
        $('#distance-miles').text(miles);
    }

    // listener for map clicks
    google.maps.event.addListener(map, 'click', function(event) {

        // check whether we are in distance measure mode
        if ($('#distance-measure-btn').hasClass('active')) {

            addDistancePoint(event.latLng);
            addDistanceListItem(event.latLng);
            calcDistance();
        }
    });

    // listener for when the user changes the points by dragging them
    google.maps.event.addListener(layers['distance'], 'directions_changed', function() {

        // update the distance amounts
        updateDistance(layers['distance'].directions);

        // clear the current distance list items and distancePoint array
        $('#distance-list').html('');
        distancePoints = [];

        // add an item for the origin
        addDistanceListItem(layers['distance'].directions.routes[0].legs[0].start_location);
        addDistancePoint(layers['distance'].directions.routes[0].legs[0].start_location);

        // if there are waypoints add an item for each of them
        if (layers['distance'].directions.routes[0].legs[0].via_waypoints.length > 0) {
            for (var i = 0; i < layers['distance'].directions.routes[0].legs[0].via_waypoints.length; i++) {
                addDistanceListItem(layers['distance'].directions.routes[0].legs[0].via_waypoints[i]);
                addDistancePoint(layers['distance'].directions.routes[0].legs[0].via_waypoints[i])
            }
        }
        // add an item for the destination
        addDistanceListItem(layers['distance'].directions.routes[0].legs[0].end_location);
        addDistancePoint(layers['distance'].directions.routes[0].legs[0].end_location)
    });

    // user clicks the clear all button
    $('#distance-clear-btn').click(function() {

        // reset the distance position array
        distancePoints = [];

        // remove the points from the list
        $('#distance-list').html('');

        // clear the overlay from the map
        layers['distance'].setMap(null);

        // set the distances to 0
        updateDistance(null);
    });
});